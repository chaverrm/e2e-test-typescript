import { browser } from 'protractor';
import { ContactPage } from '../src/pages/contact.page';
import { HomePage } from '../src/pages/home.page';

import * as fs from 'fs';

let contactPage;
let homePage;
let contactData;

describe('', () => {

    beforeAll(async () => {
        await browser.waitForAngularEnabled(false);
        contactPage = new ContactPage();
        homePage = new HomePage();
        await browser.get('https://www.eltiempo.com/');
        //contactData = JSON.parse(fs.readFileSync('./test/data/contact.json', 'utf8'));
    });
    
    beforeEach(async () => {
    });
    
    it('Send the contact form correctly', async () => {
        await homePage.closePopups();
        await homePage.goToContactPage();
    });

});
