import { by, element, ElementFinder, browser } from 'protractor';
import { BasePage } from './base.page';

export class HomePage extends BasePage{

    private _lnkContact: ElementFinder;
    private _btnAcceptCookies: ElementFinder;
    private _btnNoNotifications: ElementFinder;

    constructor () {
        super();
        this._lnkContact = element(by.linkText('CONTÁCTENOS'));
        this._btnAcceptCookies = element(by.cssContainingText('button', 'Acepto'));    
        this._btnNoNotifications = element(by.cssContainingText('button', 'NO GRACIAS'));    
    }

    public async closePopups() {
        await this.waitUntilElementToBeClickable(this._btnAcceptCookies);
        await this._btnAcceptCookies.click();
        await this.waitUntilElementVisible(this._btnNoNotifications);
        await this._btnNoNotifications.click();

    }

    public async goToContactPage () {        
        await browser.actions().mouseMove(this._lnkContact).perform();
        await this._lnkContact.click();
    }
}
