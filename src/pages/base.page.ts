import { browser, ElementFinder } from "protractor";
import { protractor } from "protractor/built/ptor";

export class BasePage {

    private DEFAULT_TIME = 15000;

    constructor() {

    }

    public async waitUntilElementVisible(element: ElementFinder, time: number = this.DEFAULT_TIME) {
        await browser.wait(browser.ExpectedConditions.visibilityOf(element), time);
    }

    public async waitUntilElementToBeClickable(element: ElementFinder, 
                                               time: number = this.DEFAULT_TIME) {
        await browser.wait(browser.ExpectedConditions.elementToBeClickable(element), time);
    }



}
