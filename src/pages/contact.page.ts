import { by, element, ElementFinder } from 'protractor';

export class ContactPage {

    private _txtName: ElementFinder;
    private _txtDocument: ElementFinder;
    private _txtEmail: ElementFinder;
    private _txtPhone: ElementFinder;
    private _txtMessage: ElementFinder;
    private _checkAgreements: ElementFinder;
    private _reCaptcha: ElementFinder;
    private _btnSend: ElementFinder;

    constructor () {
        this._txtName = element(by.id('eltiempo_cms_contact_name'));
        this._txtDocument = element(by.id('eltiempo_cms_contact_userid'));
        this._txtEmail = element(by.id('eltiempo_cms_contact_email'));
        this._txtPhone = element(by.id('eltiempo_cms_contact_telephone'));
        this._txtMessage = element(by.id('eltiempo_cms_contact_message'));
        this._checkAgreements = element(by.className('terms-false-chb'));
        this._reCaptcha = element(by.css('span[id="recaptcha-anchor"]'));
        this._btnSend = element(by.className('btn_continuar form-submit'));
    }

    public async fillForm (name: string, document: string, email: string, phone: string, message: string) {
        await this._txtName.sendKeys(name);
        await this._txtDocument.sendKeys(document);
        await this._txtEmail.sendKeys(email);
        await this._txtPhone.sendKeys(phone);
        await this._txtMessage.sendKeys(message);
        await this._checkAgreements.click();
        await this._reCaptcha.click();
        await this._btnSend.click();
    }
}
