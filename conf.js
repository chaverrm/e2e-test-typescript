const {
    SpecReporter
} = require('jasmine-spec-reporter');
exports.config = {
    directConnect: false,
    specs: ['test/contacto.spec.ts'],
    framework: 'jasmine',
    onPrepare: () => {
        browser.manage().timeouts().implicitlyWait(35000);
        require('ts-node').register({
            project: 'tsconfig.json'
        });
        jasmine.getEnv().addReporter(new SpecReporter({
            spec: {
              displayStacktrace: true
            }
          }));
    },
    capabilities: {
        browserName: 'chrome',
        shardTestFiles: true,
        maxInstances: 1,
        chromeOptions: {
            args: [
                '--disable-infobars',
                '--disable-extensions',
                'verbose',
                '--incognito',
                '--test-type',
                '--ignore-certificate-errors',
                '--allow-running-insecure-content',
                '--disk-cache-dir=null',
                '--start-maximized',
                'log-path=/tmp/chromedriver.log'
            ],
            prefs: {
                'profile.password_manager_enabled': false,
                'credentials_enable_service': false,
                'password_manager_enabled': false
            }
        }
    },
    jasmineNodeOpts: {
        showColors: true,
        displaySpecDuration: true,
        print: () => {},
        defaultTimeoutInterval: 90000
    }
};